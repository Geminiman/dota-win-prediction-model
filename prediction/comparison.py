import json
import time

import requests
from pymongo import MongoClient

from prediction.constants import (
    MONGODB_STR,
    PUBLIC_MATCHES_URL,
    MATCHES_URL,
    GAME_MODES,
)
from prediction.utils.db import find_mean
from prediction.win_probability import win_probability


client = MongoClient(MONGODB_STR)
db = client.dota_stats
counterpicks = db.counterpicks
heroes_winrate = db.heroes_winrate


def compare_by_counterpicks(bias: int = 0.53) -> None:
    with open("data/loot_bet_matches.txt", "r") as matches_file:
        wins = 0
        total = 0
        for match in matches_file:
            time.sleep(0.5)
            req_match = requests.get(f"{MATCHES_URL}{match}")
            match_parsed = json.loads(req_match.content)

            radiant_team = [match_parsed["players"][i]["hero_id"] for i in range(5)]
            dire_team = [match_parsed["players"][i]["hero_id"] for i in range(5, 10)]

            mean = find_mean(radiant_team, dire_team)
            radiant_win = match_parsed["radiant_win"]

            if mean > bias:
                total += 1
                if radiant_win:
                    print(mean, radiant_win)
                    wins += 1

            else:
                total += 1
                if not radiant_win:
                    print(mean, radiant_win)
                    wins += 1

    print("wins: {0}, total: {1}, percent: {2}".format(wins, total, wins / total))


def compare_by_winrate(bias: int = 0.15) -> None:
    total_matches_count: int = 0
    match_id_latest: int = 0
    total: int = 0
    win: int = 0
    for j in range(50):
        print(j)
        if match_id_latest == 0:
            req_public_matches = requests.get(PUBLIC_MATCHES_URL)
        else:
            req_public_matches = requests.get(
                f"{PUBLIC_MATCHES_URL}?less_than_match_id=" + str(match_id_latest)
            )
        matches = json.loads(req_public_matches.content)
        for i in range(len(matches)):
            if (
                matches[i]["duration"] > 1200
                and matches[i]["avg_mmr"] is not None
                and matches[i]["avg_mmr"] >= 5000
                and matches[i]["game_mode"] in GAME_MODES
            ):
                radiant_winrate = 0
                dire_winrate = 0
                total_matches_count += 1

                radiant_team = list(
                    map(lambda x: int(x), matches[i]["radiant_team"].split(","))
                )
                dire_team = list(
                    map(lambda x: int(x), matches[i]["dire_team"].split(","))
                )

                for k in radiant_team:
                    radiant_winrate += heroes_winrate.find_one({"hero_id": k})[
                        "winrate"
                    ]

                for k in dire_team:
                    dire_winrate += heroes_winrate.find_one({"hero_id": k})["winrate"]

                if (radiant_winrate / 5 - dire_winrate / 5) > bias:
                    total += 1
                    if matches[i]["radiant_win"]:
                        win += 1
                if (dire_winrate / 5 - radiant_winrate / 5) > bias:
                    total += 1
                    if not matches[i]["radiant_win"]:
                        win += 1
            match_id_latest = matches[i]["match_id"]
    print("wins:{0}".format(win / total))


def compare_by_integrated_model(bias: int = 0.05) -> None:
    total_matches_count: int = 0
    match_id_latest: int = 0
    total: int = 0
    win: int = 0
    for j in range(50):
        print(j)
        if match_id_latest == 0:
            req_public_matches = requests.get(PUBLIC_MATCHES_URL)
        else:
            req_public_matches = requests.get(
                f"{PUBLIC_MATCHES_URL}?less_than_match_id=" + str(match_id_latest)
            )
        matches = json.loads(req_public_matches.content)
        for i in range(len(matches)):
            if (
                matches[i]["duration"] > 1200
                and matches[i]["avg_mmr"] is not None
                and matches[i]["avg_mmr"] >= 5000
                and matches[i]["game_mode"] in GAME_MODES
            ):
                total_matches_count += 1

                radiant_team = list(
                    map(lambda x: int(x), matches[i]["radiant_team"].split(","))
                )
                dire_team = list(
                    map(lambda x: int(x), matches[i]["dire_team"].split(","))
                )

                if win_probability(radiant_team, dire_team) > bias:
                    total += 1
                    if matches[i]["radiant_win"]:
                        win += 1
                if win_probability(radiant_team, dire_team) > bias:
                    total += 1
                    if not matches[i]["radiant_win"]:
                        win += 1
               

            match_id_latest = matches[i]["match_id"]
    print("wins:{0}".format(win / total))


if __name__ == "__main__":
    compare_by_integrated_model()
