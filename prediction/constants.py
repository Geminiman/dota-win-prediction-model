MONGODB_STR = "mongodb://localhost:27017/"

HEROES_URL = "https://api.opendota.com/api/heroes/"
PUBLIC_MATCHES_URL = "https://api.opendota.com/api/publicMatches/"
MATCHES_URL = "https://api.opendota.com/api/matches/"

GAME_MODES = (1, 2, 22)
