import asyncio

from pymongo import MongoClient
from codetiming import Timer

from prediction.constants import MONGODB_STR
from prediction.prediction_models.damage_model import DamageBasedModel

if __name__ == "__main__":
    pass

    timer = Timer(text=f" elapsed time: {{:.1f}}")
    client = MongoClient(MONGODB_STR)
    db = client.dota_stats
    DBM = DamageBasedModel(db)
    # DBM.get_matches(5000)
    # timer.start()
    # asyncio.run(DBM.run_downloading())
    # timer.stop()
    print(DBM.correlation_test2())
    print(DBM.db_records_count())

