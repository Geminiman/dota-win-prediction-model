from functools import reduce
from typing import Dict, List
from prediction.utils.math import algebraic_sum
from pymongo import MongoClient

client = MongoClient("mongodb://localhost:27017/")
db = client.dota_stats
counterpicks = db.counterpicks
heroes_winrate = db.heroes_winrate


def win_probability(radiant_team: List[int], dire_team: List[int]) -> str:
    # 1. get winrates.
    radiant_winrate: List[float] = []
    dire_winrate: List[float] = []
    for r in radiant_team:
        radiant_winrate.append(heroes_winrate.find_one({"hero_id": r})["winrate"] / 100)

    for r in dire_team:
        dire_winrate.append(heroes_winrate.find_one({"hero_id": r})["winrate"] / 100)

    # 2. get counterpicks.
    radiant_counter: List[float] = []
    dire_counter: List[float] = []
    # 2. a, radiant
    try:
        for r in radiant_team:
            score_hero = []

            for d in dire_team:
                if r < d:
                    score = counterpicks.find_one({"hero_id_1": r, "hero_id_2": d})[
                        "score"
                    ]
                else:
                    score = counterpicks.find_one({"hero_id_1": d, "hero_id_2": r})[
                        "score"
                    ]
                score_hero.append(score)

            score_hero_percent = 0
            for hero in score_hero:
                score_hero_percent += round(hero[0] / (hero[0] + hero[1]), 3)
            radiant_counter.append(round(score_hero_percent / 5, 3))
    except:
        print(r, d)

    # 2.b, dire
    for r in dire_team:
        score_hero = []

        for d in radiant_team:
            if r < d:
                score = counterpicks.find_one({"hero_id_1": r, "hero_id_2": d})["score"]
            else:
                score = counterpicks.find_one({"hero_id_1": d, "hero_id_2": r})["score"]
            score_hero.append(score)

        score_hero_percent = 0
        for hero in score_hero:
            score_hero_percent += round(hero[0] / (hero[0] + hero[1]), 3)
        dire_counter.append(round(score_hero_percent / 5, 3))

    # 3. Calculating algebraic sum.
    radiant_alg = list(
        map(lambda x, y: algebraic_sum(x, y), radiant_counter, radiant_winrate)
    )
    dire_alg = list(map(lambda x, y: algebraic_sum(x, y), dire_counter, dire_winrate))

    # 4. Calculating mean.
    mean_radiant = sum(radiant_alg) / 5
    mean_dir = sum(dire_alg) / 5

    # 5. Calculating difference.
    diff = round(mean_radiant - mean_dir, 3)

    return diff


if __name__ == "__main__":
    radiant_team = [10, 7, 19, 121, 73]
    dire_team = [20, 52, 53, 14, 114]
    print(win_probability(dire_team, radiant_team))
