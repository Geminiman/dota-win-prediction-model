import argparse

from pymongo import MongoClient

from prediction.constants import MONGODB_STR
from prediction.prediction_models.damage_model import DamageBasedModel


def damage_based_model_cli(args):

    client = MongoClient(MONGODB_STR)
    db = client.dota_stats
    DBM = DamageBasedModel(db)
    actions = {"count": DBM.db_records_count}
    if args.action in actions.keys():
        print(actions[args.action]())


def utils_cli(args, model_name_parser):
    parser = argparse.ArgumentParser(parents=[model_name_parser])
    parser.add_argument("task", type=str, help="model action")
    print(parser.parse_args())

def models_cli(args, model_name_parser):
    parser = argparse.ArgumentParser(parents=[model_name_parser])

    parser.add_argument("action", type=str, help="model action")
    pass


def main_cli():
    # models = ["damage_based"]
    actions = ["utils", "models"]
    actions = {"utils": utils_cli, "models": models_cli}

    model_name_parser = argparse.ArgumentParser(description="Dota prediction models", add_help=False)
    model_name_parser.add_argument("action", type=str, help="action name")


    # parser.add_argument("collection_name", type=str, help="mongodb collection name")
    args = model_name_parser.parse_args()
    if args.action in actions:
        actions[args.action](args, model_name_parser)

main_cli()
