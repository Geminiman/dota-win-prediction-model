import asyncio
import json
import time
from typing import Dict, List

import aiohttp
import requests
from scipy.stats.stats import pearsonr

from prediction.constants import GAME_MODES, MATCHES_URL, PUBLIC_MATCHES_URL
from prediction.utils.parsing import ParsingUtils


class NetworkErrorException(Exception):
    pass


class DamageBasedModel:
    def __init__(self, db_instance) -> None:
        self.__db_instance = db_instance
        self.__heroes_damage_matches = db_instance.heroes_damage_matches
        self.__heroes_damage_stats = db_instance.heroes_damage_stats
        self.__latest_match = db_instance.latest_match

    # Save hero damage stats data to db.
    def __save_heroes_damage_stats(self, heroes_damage_stats_list: List[List]) -> None:
        for hero_damage_stats_list in heroes_damage_stats_list:
            self.__heroes_damage_stats.insert_one(
                {
                    "hero_id": hero_damage_stats_list[0],
                    "damage": hero_damage_stats_list[1],
                    "damage_per_min": hero_damage_stats_list[2],
                    "healing_per_min": hero_damage_stats_list[3],
                    "damage_taken": hero_damage_stats_list[4],
                }
            )

    # Download list of matches which fit the conditions.
    def get_matches(self, matches_count: int = 10) -> None:
        if self.__latest_match.count() > 0:
            latest_match_id = self.__latest_match.find_one()["match_id"]
        else:
            latest_match_id = None
        for _ in range(matches_count):
            if latest_match_id:
                query_params_string = (
                    f"less_than_match_id={latest_match_id}#mmr_descending=true"
                )
            else:
                query_params_string = "mmr_descending=true"
            public_matches_request = requests.get(
                f"{PUBLIC_MATCHES_URL}?{query_params_string}"
            )
            if public_matches_request.ok:
                matches_json = json.loads(public_matches_request.content)

                for i in range(len(matches_json)):
                    if (
                        matches_json[i]["duration"] >= 1200
                        and matches_json[i]["avg_mmr"] is not None
                        and matches_json[i]["avg_mmr"] >= 5500
                        and matches_json[i]["game_mode"] in GAME_MODES
                    ):
                        if not self.__heroes_damage_matches.find_one(
                            {"match_id": matches_json[i]["match_id"]}
                        ):
                            db_record: Dict = {
                                "match_id": matches_json[i]["match_id"],
                                "processed": False,
                            }
                            self.__heroes_damage_matches.insert_one(db_record)
                latest_match_id = matches_json[i]["match_id"]

            elif public_matches_request.status_code == 429:
                print("Sleeping...")
                time.sleep(60)

            else:
                raise NetworkErrorException
        self.__latest_match.drop()
        self.__latest_match.insert_one({"match_id": latest_match_id})

    # Download hero damage data.
    async def download_hero_damage_and_healing_data(self, work_queue):
        async with aiohttp.ClientSession() as session:
            while not work_queue.empty():
                match_url = await work_queue.get()
                async with session.get(match_url) as response:
                    if response.status == 200:
                        match_json = await response.json()
                        heroes_damage_stats_list: List[List] = []

                        for player_record in match_json["players"]:
                            if player_record["damage_taken"]:
                                total_damage_taken: int = 0
                                for hero_name, damage_taken in player_record[
                                    "damage_taken"
                                ].items():
                                    if "npc_dota_hero" in hero_name:
                                        total_damage_taken += damage_taken
                                hero_damage_stats_list = [
                                    player_record["hero_id"],
                                    player_record["hero_damage"],
                                    player_record["benchmarks"]["hero_damage_per_min"][
                                        "raw"
                                    ],
                                    player_record["benchmarks"]["hero_healing_per_min"][
                                        "raw"
                                    ],
                                    total_damage_taken,
                                ]
                                heroes_damage_stats_list.append(hero_damage_stats_list)

                        self.__save_heroes_damage_stats(heroes_damage_stats_list)
                    elif response.status == 429:
                        print("Sleeping...")
                        time.sleep(60)

                    else:
                        raise NetworkErrorException

    async def run_downloading(self) -> None:
        work_queue = asyncio.Queue()
        for match in self.__heroes_damage_matches.find():
            match_url: str = f"{MATCHES_URL}{match['match_id']}"
            await work_queue.put(match_url)

        await asyncio.gather(
            asyncio.create_task(self.download_hero_damage_and_healing_data(work_queue)),
            asyncio.create_task(self.download_hero_damage_and_healing_data(work_queue)),
            asyncio.create_task(self.download_hero_damage_and_healing_data(work_queue)),
            asyncio.create_task(self.download_hero_damage_and_healing_data(work_queue)),
        )

    def db_records_count(self) -> int:
        return self.__heroes_damage_matches.count_documents({})

    def drop_db_collection(self, collection_name: str) -> None:
        self.__db_instance.drop_collection(collection_name)
        print(f"{collection_name} dropped")

    # Test, based on damage and taken damage of both teams.
    def correlation_test(self):
        heroes_avg_damage_dict: Dict[str, int] = {}
        for db_record in self.__heroes_damage_stats.aggregate(
            [
                {
                    "$group": {
                        "_id": {"hero_id": "$hero_id"},
                        "avg_damage": {"$avg": "$damage"},
                    }
                }
            ]
        ):
            heroes_avg_damage_dict[str(db_record["_id"]["hero_id"])] = db_record[
                "avg_damage"
            ]

        heroes_avg_damage_taken_dict: Dict[str, int] = {}
        for db_record in self.__heroes_damage_stats.aggregate(
            [
                {
                    "$group": {
                        "_id": {"hero_id": "$hero_id"},
                        "avg_damage_taken": {"$avg": "$damage_taken"},
                    }
                }
            ]
        ):
            heroes_avg_damage_taken_dict[str(db_record["_id"]["hero_id"])] = db_record[
                "avg_damage_taken"
            ]
        league_name = "summit_11"
        league_data: List[Dict] = ParsingUtils.get_heroes_and_result_status(league_name)

        match_result_list: List = []
        damage_diff_list: List = []
        for match in league_data:

            match_result_list.append(match["radiant_win"])
            radiant_avg_damage = 0
            dire_avg_damage = 0
            for hero_id in match["radiant_team"]:
                radiant_avg_damage += heroes_avg_damage_dict[str(hero_id)]

            for hero_id in match["dire_team"]:
                dire_avg_damage += heroes_avg_damage_dict[str(hero_id)]

            radiant_avg_damage_taken = 0
            dire_avg_damage_taken = 0
            for hero_id in match["radiant_team"]:
                radiant_avg_damage_taken += heroes_avg_damage_taken_dict[str(hero_id)]

            for hero_id in match["dire_team"]:
                dire_avg_damage_taken += heroes_avg_damage_taken_dict[str(hero_id)]

            damage_diff_list.append(
                radiant_avg_damage
                - dire_avg_damage
                - (dire_avg_damage - radiant_avg_damage_taken)
            )

        damage_diff_list = [x / abs(x) if x != 0 else x for x in damage_diff_list]

        return pearsonr(match_result_list, damage_diff_list)

    def correlation_test2(self):
        heroes_avg_damage_dict: Dict[str, int] = {}
        for db_record in self.__heroes_damage_stats.aggregate(
            [
                {
                    "$group": {
                        "_id": {"hero_id": "$hero_id"},
                        "avg_damage_per_min": {"$avg": "$damage_per_min"},
                    }
                }
            ]
        ):
            heroes_avg_damage_dict[str(db_record["_id"]["hero_id"])] = db_record[
                "avg_damage_per_min"
            ]

        heroes_avg_damage_taken_dict: Dict[str, int] = {}
        for db_record in self.__heroes_damage_stats.aggregate(
            [
                {
                    "$group": {
                        "_id": {"hero_id": "$hero_id"},
                        "avg_healing_per_min": {"$avg": "$healing_per_min"},
                    }
                }
            ]
        ):
            heroes_avg_damage_taken_dict[str(db_record["_id"]["hero_id"])] = db_record[
                "avg_healing_per_min"
            ]
        league_name = "summit_11"
        league_data: List[Dict] = ParsingUtils.get_heroes_and_result_status(league_name)

        match_result_list: List = []
        damage_diff_list: List = []
        for match in league_data:

            match_result_list.append(match["radiant_win"])
            radiant_avg_damage = 0
            dire_avg_damage = 0
            for hero_id in match["radiant_team"]:
                radiant_avg_damage += heroes_avg_damage_dict[str(hero_id)]

            for hero_id in match["dire_team"]:
                dire_avg_damage += heroes_avg_damage_dict[str(hero_id)]

            radiant_avg_healing = 0
            dire_avg_healing = 0
            for hero_id in match["radiant_team"]:
                radiant_avg_healing += heroes_avg_damage_taken_dict[str(hero_id)]

            for hero_id in match["dire_team"]:
                dire_avg_healing += heroes_avg_damage_taken_dict[str(hero_id)]

            damage_diff_list.append(
                radiant_avg_damage - dire_avg_healing
                - (dire_avg_damage - radiant_avg_healing)
            )
        damage_diff_list = [x / abs(x) if x != 0 else x for x in damage_diff_list]

        return pearsonr(match_result_list, damage_diff_list)
