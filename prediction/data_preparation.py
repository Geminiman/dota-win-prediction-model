import json
import time
from itertools import combinations, product
from typing import Dict, List

import requests
from bs4 import BeautifulSoup as bs
from pymongo import MongoClient

from prediction.constants import MONGODB_STR, PUBLIC_MATCHES_URL

client = MongoClient(MONGODB_STR)
db = client.dota_stats

matches_coll = db.matches
counterpicks_coll = db.counterpicks
pairs_coll = db.pairs
heroes_winrate_coll = db.heroes_winrate
heroes_damage_stats_coll = db.heroes_damage_stats


# Save counterpick pairs to mongo.
def save_counterpick_db(records: List):
    for record in records:
        tmp = counterpicks_coll.find_one(
            {"hero_id_1": record["hero_id_1"], "hero_id_2": record["hero_id_2"]}
        )
        if tmp is None:
            counterpicks_coll.insert_one(record)
        else:
            counterpicks_coll.find_one_and_update(
                {"hero_id_1": record["hero_id_1"], "hero_id_2": record["hero_id_2"]},
                {
                    "$set": {
                        "score": [
                            record["score"][0] + tmp["score"][0],
                            record["score"][1] + tmp["score"][1],
                        ]
                    }
                },
            )


# Check does match exists or not in mongo.
def check_match(match_id: int) -> bool:
    tmp = matches_coll.find_one({"match_id": match_id})
    if tmp is not None:
        return False
    else:
        matches_coll.insert_one({"match_id": match_id})
        return True


# Calculate score of counterpick pair per match.
def calc_countrepick_score(match_result) -> List:
    heroes_cartesian: List[int] = []
    score_cartesian: List[int] = []
    result: List = []

    radiant_heroes = list(
        map(lambda x: int(x), match_result["radiant_team"].split(","))
    )
    dire_heroes = list(map(lambda x: int(x), match_result["dire_team"].split(",")))
    score = list(
        map(
            lambda x: abs(x - match_result["radiant_win"]),
            [1, 1, 1, 1, 1, 0, 0, 0, 0, 0],
        )
    )

    for i in product(radiant_heroes, dire_heroes):
        heroes_cartesian.append(i)

    for i in product(score[:5], score[5:]):
        score_cartesian.append(i)

    for i in range(len(heroes_cartesian)):
        if heroes_cartesian[i][0] < heroes_cartesian[i][1]:
            result.append(
                {
                    "hero_id_1": heroes_cartesian[i][0],
                    "hero_id_2": heroes_cartesian[i][1],
                    "score": [score_cartesian[i][0], score_cartesian[i][1]],
                }
            )
        else:
            result.append(
                {
                    "hero_id_1": heroes_cartesian[i][1],
                    "hero_id_2": heroes_cartesian[i][0],
                    "score": [score_cartesian[i][1], score_cartesian[i][0]],
                }
            )
    return result


# creating heroes counterpick database from open matches.
def counterpicks_forming(avg_mmr: int, match_duration: int) -> None:
    match_id_latest: int = 0
    for j in range(10000):
        print(j)
        if match_id_latest == 0:
            req_public_matches = requests.get(PUBLIC_MATCHES_URL)
        else:
            req_public_matches = requests.get(
                f"{PUBLIC_MATCHES_URL}?less_than_match_id={match_id_latest}"
            )
        matches = json.loads(req_public_matches.content)

        for i in range(len(matches)):
            if (
                check_match(matches[i]["match_id"])
                and (matches[i]["duration"] >= match_duration)
                and (matches[i]["avg_mmr"] is not None)
                and (matches[i]["avg_mmr"] >= avg_mmr)
                and matches[i]["game_mode"] in (1, 2, 22)
            ):
                result = calc_countrepick_score(matches[i])
                save_counterpick_db(result)
        
        match_id_latest = matches[i]["match_id"]
        time.sleep(0.7)


# calculate score of hero pairs per match.
def calc_hero_pair_score(match_result) -> List:
    result: List = []

    radiant_heroes = list(
        map(lambda x: int(x), match_result["radiant_team"].split(","))
    )
    dire_heroes = list(map(lambda x: int(x), match_result["dire_team"].split(",")))
    radiant_heroes.sort()
    dire_heroes.sort()

    radiant_comb = list(combinations(radiant_heroes, 2))
    dire_comb = list(combinations(dire_heroes, 2))

    for i in range(3):
        if match_result["radiant_win"]:
            result.append(
                {
                    "hero_id_1": radiant_comb[i][0],
                    "hero_id_2": radiant_comb[i][1],
                    "score": [1, 1],
                }
            )
            result.append(
                {
                    "hero_id_1": dire_comb[i][0],
                    "hero_id_2": dire_comb[i][1],
                    "score": [0, 1],
                }
            )
        else:
            result.append(
                {
                    "hero_id_1": radiant_comb[i][0],
                    "hero_id_2": radiant_comb[i][1],
                    "score": [0, 1],
                }
            )
            result.append(
                {
                    "hero_id_1": dire_comb[i][0],
                    "hero_id_2": dire_comb[i][1],
                    "score": [1, 1],
                }
            )
    return result


# get all hero pairs in match and save their score.
def save_hero_pairs_db(records: List, win: bool) -> None:
    for record in records:
        tmp = pairs_coll.find_one(
            {"hero_id_1": record["hero_id_1"], "hero_id_2": record["hero_id_2"]}
        )
        if tmp is None:
            pairs_coll.insert_one(record)
        else:
            if win:
                pairs_coll.find_one_and_update(
                    {
                        "hero_id_1": record["hero_id_1"],
                        "hero_id_2": record["hero_id_2"],
                    },
                    {
                        "$set": {
                            "score": [
                                record["score"][0] + tmp["score"][0],
                                record["score"][1] + tmp["score"][1],
                            ]
                        }
                    },
                )
            else:
                pairs_coll.find_one_and_update(
                    {
                        "hero_id_1": record["hero_id_1"],
                        "hero_id_2": record["hero_id_2"],
                    },
                    {
                        "$set": {
                            "score": [
                                tmp["score"][0],
                                record["score"][1] + tmp["score"][1],
                            ]
                        }
                    },
                )


def hero_pairs_forming(avg_mmr: int, match_duration: int) -> None:
    match_id_latest: int = 0
    for j in range(1000):
        print(j)
        if match_id_latest == 0:
            req_public_matches = requests.get(PUBLIC_MATCHES_URL)
            matches = json.loads(req_public_matches.content)
        else:
            req_public_matches = requests.get(
                f"{PUBLIC_MATCHES_URL}?less_than_match_id={match_id_latest}"
            )
            matches = json.loads(req_public_matches.content)

        for i in range(len(matches)):
            if (
                check_match(matches[i]["match_id"])
                and (matches[i]["duration"] >= match_duration)
                and (matches[i]["avg_mmr"] is not None)
                and (matches[i]["avg_mmr"] >= avg_mmr)
                and (matches[i]["game_mode"] in (1, 2, 22))
            ):
                result = calc_hero_pair_score(matches[i])
                save_hero_pairs_db(result, matches[i]["radiant_win"])
        match_id_latest = matches[i]["match_id"]


# get heroes winrate in current meta from dotabuff.
def get_heroes_winrate():
    winrates: List[float] = []
    hero_names: List[str] = []
    heroes: Dict = {}

    dotabuff_html = open("data/dotabuff_meta_winrate.html").read()
    soup = bs(dotabuff_html, "html.parser")

    for td in soup.select('td[class*="r-tab r-group-5"]'):
        winrate_value = float(td["data-value"])
        if winrate_value > 32:
            winrates.append(winrate_value)
    for div in soup.find_all(
        "div", class_="image-container image-container-hero image-container-icon"
    ):
        hero_name = div.find("a")["href"].replace("/heroes/", "")
        hero_names.append(hero_name)

    with open("data/heroes.txt") as heroes_file:
        for line in heroes_file:
            line_parsed = line.split(",")
            hero_name = (
                line_parsed[1]
                .replace("\n", "")
                .lower()
                .strip()
                .replace(" ", "-")
                .replace("'", "")
            )
            heroes[hero_name] = int(line_parsed[0])
    print(heroes)
    print(hero_names)
    for i in range(len(hero_names)):
        heroes_winrate_coll.insert(
            {
                "hero_id": heroes[hero_names[i].lower()],
                "hero_name": hero_names[i].lower(),
                "winrate": winrates[i],
            }
        )

    print("ok")


if __name__ == "__main__":
    counterpicks_forming(4500, 1200)
