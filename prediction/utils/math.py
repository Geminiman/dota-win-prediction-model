def algebraic_sum(x: int, y: int) -> float:
    return x + y - x * y