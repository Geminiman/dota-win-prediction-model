from typing import List

from pymongo import MongoClient

from prediction.constants import MONGODB_STR

client = MongoClient(MONGODB_STR)
db = client.dota_stats

heroes_winrate = db.heroes_winrate
counterpicks = db.counterpicks


class DBUtils:
    def __init__(self, db_instance):
        self.__db_instance = db_instance

    @staticmethod
    def find_mean(radiant_team, dire_team):
        total_mean = []
        for r in radiant_team:
            score_hero = []

            for d in dire_team:
                if r < d:
                    score = counterpicks.find_one({"hero_id_1": r, "hero_id_2": d})[
                        "score"
                    ]
                else:
                    score = counterpicks.find_one({"hero_id_1": d, "hero_id_2": r})[
                        "score"
                    ]
                score_hero.append(score)

            score_hero_percent = 0
            for hero in score_hero:
                score_hero_percent += round(hero[0] / (hero[0] + hero[1]), 3)
            total_mean.append(round(score_hero_percent / 5, 3))

        print(total_mean)
        return round(sum(total_mean) / 5, 3)

    @staticmethod
    def get_winrate(radiant_team, dire_team):
        radiant_team_winrate: List[float] = []
        dire_team_winrate: List[float] = []
        for r in radiant_team:
            radiant_team_winrate.append(
                heroes_winrate.find_one({"hero_id": r})["winrate"]
            )

        for r in dire_team:
            dire_team_winrate.append(heroes_winrate.find_one({"hero_id": r})["winrate"])

        print(
            "radiant winrate: {0}, mean: {1}".format(
                radiant_team_winrate, sum(radiant_team_winrate) / 5
            )
        )
        print(
            "dire winrate: {0}, mean: {1}".format(
                dire_team_winrate, sum(dire_team_winrate) / 5
            )
        )

    def db_records_count(self, collection_name: str) -> int:
        return self.__db_instance[collection_name].count_documents({})

    def drop_db_collection(self, collection_name: str) -> None:
        self.__db_instance.drop_collection(collection_name)
        print(f"{collection_name} dropped")
