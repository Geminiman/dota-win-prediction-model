import json
import pathlib
import subprocess
from typing import Set, List, Dict

import requests
from bs4 import BeautifulSoup as bs

from prediction.constants import HEROES_URL, MATCHES_URL


class ParsingUtils:
    # Download all dotabuff pages with games urls for certain league.
    @staticmethod
    def download_dotabuff_pages(last_page_url: str, league_name: str):
        last_page_number: int = int(last_page_url[-1])
        folder = f"data/{league_name}/"
        for page_number in range(1, last_page_number + 1):
            subprocess.call(
                [
                    "wget",
                    f"{last_page_url[:-1]}{page_number}",
                    "-O",
                    f"{folder}{league_name}_page{page_number}.html",
                ]
            )
        print(f"Downloaded {last_page_number} pages")

    # Parsing downloaded dotabuff pages.
    @staticmethod
    def get_matches_from_dotabuff_pages(league_name: str) -> Set[int]:
        folder = f"data/{league_name}/"
        matches_set: set = set()
        for html_file in [p for p in pathlib.Path(folder).iterdir() if p.is_file()]:
            dotabuff_html = open(html_file, "r").read()
            soup = bs(dotabuff_html, "html.parser")

            for span in soup.find_all("span", class_="icon complete"):
                match_number = bs(str(span), "html.parser").find("a", href=True)["href"]
                matches_set.add(int(match_number.replace("/matches/", "")))
        return matches_set

    # get list of heroes id and win/loose status for each team.
    # TODO: fix different results each time.
    @staticmethod
    def get_heroes_and_result_status(league_name: str) -> List[Dict]:
        matches = ParsingUtils.get_matches_from_dotabuff_pages(league_name)
        result_list: List[Dict] = []
        for match in matches:
            match_url: str = f"{MATCHES_URL}{match}"
            match_request = requests.get(match_url)
            heroes_id_list: List = []
            if match_request.ok:
                match_json = json.loads(match_request.content)
                radiant_win = 1 if match_json["radiant_win"] else -1
                for player_record in match_json["players"]:
                    heroes_id_list.append(player_record["hero_id"])
                result_dict = {
                    "match_id": match,
                    "radiant_win": radiant_win,
                    "radiant_team": heroes_id_list[:5],
                    "dire_team": heroes_id_list[5:],
                }
                result_list.append(result_dict)
        return result_list

    # get all heroes id and name
    @staticmethod
    def get_heroes_data():
        with open("data/heroes.txt", "w") as heroes_file:
            req_heroes = requests.get(HEROES_URL)
            heroes_parsed = json.loads(req_heroes.content)
            for hero in heroes_parsed:
                heroes_file.write(
                    "{0}, {1}\n".format(hero["id"], hero["localized_name"])
                )

    @staticmethod
    def get_esport_matches():
        dotabuff_html = open("data/dotabuff.html").read()
        soup = bs(dotabuff_html, "html.parser")

        with open("data/loot_bet_matches.txt", "a") as matches_file:
            matches_set = set()
            for span in soup.find_all("span", class_="icon complete"):
                match_number = bs(str(span), "html.parser").find("a", href=True)["href"]
                matches_set.add(int(match_number.replace("/matches/", "")))

            for match in matches_set:
                matches_file.write("{0}\n".format(match))
